package com.enefce.xkcd.features.comics.list

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.enefce.xkcd.R
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ComicListFragmentTest {
    private lateinit var navController: TestNavHostController
    private lateinit var directions: NavDirections
    private lateinit var comicListScenario: FragmentScenario<ComicListFragment>

    @Before
    fun setup() {
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        navController.setGraph(R.navigation.nav_graph)
        navController.setCurrentDestination(R.id.listFragment)
        directions = ComicListFragmentDirections.actionListFragmentToDetailFragment()
        comicListScenario =
            launchFragmentInContainer<ComicListFragment>(themeResId = R.style.AppTheme)
        comicListScenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.view!!, navController)
        }
    }


    @Test
    fun launchComicListFragmentandVerifyUI() {
        onView(withId(R.id.comic_list))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.progress))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.error_tv))
            .check(ViewAssertions.matches(Matchers.not(ViewMatchers.isDisplayed())))
    }


}