package com.enefce.xkcd.features.comics.detail

import android.os.Bundle
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.enefce.xkcd.R
import com.enefce.xkcd.data.model.ComicStrip
import com.enefce.xkcd.features.comics.detail.ComicDetailFragment.Companion.ARG_ID
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ComicDetailFragmentTest {
    private lateinit var comicDetailScenario: FragmentScenario<ComicDetailFragment>
    val bundle = Bundle().apply {
        putInt(ARG_ID, 1)
    }

    private var comic: ComicStrip? = null

    @Before
    fun setup() {
        comic = ComicStrip(num = 1, title = "Comic Title")
        comicDetailScenario =
            launchFragmentInContainer<ComicDetailFragment>(bundle, themeResId = R.style.AppTheme)
    }


    @Test
    fun launchComicDetailFragmentandVerifyUI() {
        onView(withId(R.id.img_comic_detail))
            .check(matches(isDisplayed()))
        onView(withId(R.id.detail_title))
            .check(matches(isDisplayed()))
        onView(withId(R.id.img_detail_fav))
            .check(matches(not(isDisplayed())))
        onView(withId(R.id.detail_desc))
            .check(matches(isDisplayed()))
    }

}