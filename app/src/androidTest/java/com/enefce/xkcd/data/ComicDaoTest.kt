package com.enefce.xkcd.data

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.enefce.xkcd.data.model.ComicStrip
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ComicDaoTest {

    private lateinit var comicDatabase: ComicDatabase

    @Before
    fun initDb() {
        comicDatabase = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            ComicDatabase::class.java
        ).build()
    }

    @After
    fun closeDb() {
        comicDatabase.clearAllTables()
        comicDatabase.close()
    }


    @Test
    fun insertComicsSavesData() = runBlocking {
        comicDatabase.comicDao().insertAll(listOf(ComicStrip(num = 1), ComicStrip(num = 2)))
        val comics = comicDatabase.comicDao().getComics()
        assertEquals(comics.size, 2)
    }

    @Test
    fun updateComicsUpdatesData() = runBlocking {
        comicDatabase.comicDao().insertAll(
            listOf(
                ComicStrip(num = 1, title = "One"),
                ComicStrip(num = 2),
                ComicStrip(num = 3)
            )
        )
        var comic = comicDatabase.comicDao().getComic(1)
        assertEquals(comic.title, "One")
        comicDatabase.comicDao().update(ComicStrip(num = 1, title = "OneUpdated"))
        comic = comicDatabase.comicDao().getComic(1)
        assertEquals(comic.title, "OneUpdated")
    }

}