package com.enefce.xkcd.data

import com.enefce.xkcd.data.model.ComicStrip

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.HttpException


@RunWith(JUnit4::class)
class ComicItemsRepositoryTest {

    private lateinit var comicDao: ComicDao
    private lateinit var comicItemService: ComicItemService
    private lateinit var comicItemsRepository: ComicItemsRepository

    private val comicItem = ComicStrip(num = 2)
    private val comicItems = listOf(ComicStrip(num = 1), ComicStrip(num = 0))
    private val getComicItemSuccessResponse = Response.success(comicItem)
    private val getComicItemsErrorResponse = Response.error("Unknown Exception", null)
    private lateinit var mockException: HttpException

    @Before
    fun setUp() {
        comicDao = mock()
        comicItemService = mock()
        mockException = mock()
        whenever(mockException.code()).thenReturn(404)

        comicItemsRepository = ComicItemsRepository(
            comicDao,
            comicItemService
        )
    }


    /*
    Test if the call returns 2 objects list
     */
    @Test
    fun testGetComicFromNetworkReturnsComicObject() =
        runBlocking {
            whenever(comicItemService.getComicItem(2)).thenReturn(comicItem)
            assertEquals(
                getComicItemSuccessResponse,
                comicItemsRepository.getComicFromNetwork(2)
            )
            assertEquals(comicItemsRepository.getComicFromNetwork(2).data?.num, 2)

        }

    /*
    Test if the call returns error
     */
    @Test
    fun testGetComicItemsReturnsError() =
        runBlocking {
            whenever(comicItemService.getRecentComicItem()).thenThrow(mockException)
            assertEquals(
                getComicItemsErrorResponse.status,
                comicItemsRepository.getComicsFromNetwork().status
            )
        }
}