package com.enefce.xkcd.features.comics.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.enefce.xkcd.data.ComicItemsRepository
import com.enefce.xkcd.data.Response
import com.enefce.xkcd.data.model.ComicStrip
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.timeout
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class ComicListViewModelTest {

    private lateinit var viewModel: ComicListViewModel
    private lateinit var comicItemsRepository: ComicItemsRepository
    private lateinit var comicItemsObserver: Observer<HashMap<Int, ComicStrip>>
    private lateinit var errorMessageObserver: Observer<String>

    private val getComicItemsEmptyFromDBResponse: Response<HashMap<Int, ComicStrip>> =
        Response.success(
            hashMapOf()
        )


    private val getComicItemsSuccessResponse: Response<HashMap<Int, ComicStrip>> =
        Response.success(
            hashMapOf(
                1 to ComicStrip(num = 1),
                2 to ComicStrip(num = 2),
                3 to ComicStrip(num = 3)
            )
        )

    private val getComicItemsErrorResponse = Response.error("Test Error", null)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")


    @ExperimentalCoroutinesApi
    @ObsoleteCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        comicItemsRepository = mock()
        viewModel = ComicListViewModel(comicItemsRepository)
        comicItemsObserver = mock()
        errorMessageObserver = mock()

    }

    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun `check if getComics is updated with success`() = runBlocking {
        whenever(comicItemsRepository.getComicsFromDB()).thenReturn(getComicItemsEmptyFromDBResponse)
        whenever(comicItemsRepository.getComicsFromNetwork()).thenReturn(
            getComicItemsSuccessResponse
        )
        viewModel.comicItemsLiveData.observeForever(comicItemsObserver)
        delay(10)
        viewModel.getComics()
        verify(comicItemsObserver, timeout(10)).onChanged(getComicItemsSuccessResponse.data)
    }

    @Test
    fun `check if errorLiveData is updated with error when getComics from Network fails`() =
        runBlocking {
            whenever(comicItemsRepository.getComicsFromDB()).thenReturn(
                getComicItemsEmptyFromDBResponse
            ) // Returns empty hashmap
            whenever(comicItemsRepository.getComicsFromNetwork()).thenReturn(
                getComicItemsErrorResponse
            )
            viewModel.errorLiveData.observeForever(errorMessageObserver)
            delay(10)
            viewModel.getComics()
            delay(10)
            verify(errorMessageObserver, timeout(10)).onChanged(getComicItemsErrorResponse.message)
        }


}