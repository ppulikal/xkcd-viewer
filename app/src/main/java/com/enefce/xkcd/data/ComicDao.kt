package com.enefce.xkcd.data

import androidx.room.*
import com.enefce.xkcd.data.model.ComicStrip

@Dao
interface ComicDao {

    @Query("SELECT * FROM comics ORDER BY num DESC")
    suspend fun getComics(): List<ComicStrip>

    @Query("SELECT * FROM comics WHERE num == :num")
    suspend fun getComic(num: Int): ComicStrip

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(comics: List<ComicStrip>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(comic: ComicStrip)
}