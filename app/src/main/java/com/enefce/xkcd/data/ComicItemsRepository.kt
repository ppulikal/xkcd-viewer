package com.enefce.xkcd.data

import com.enefce.xkcd.data.model.ComicStrip


class ComicItemsRepository(
    private val comicDao: ComicDao,
    private val comicService: ComicItemService
) {

    suspend fun getComicsFromDB(): Response<HashMap<Int, ComicStrip>> {
        return try {
            return Response.success(
                comicDao.getComics().associateBy({ it.num }, { it })
            ) as Response<HashMap<Int, ComicStrip>>
        } catch (e: Exception) {
            handleError(e)
        }
    }

    suspend fun getComicFromDB(num: Int): Response<ComicStrip> {
        return try {
            return Response.success(comicDao.getComic(num))
        } catch (e: Exception) {
            handleError(e)
        }
    }

    suspend fun getComicsFromNetwork(): Response<HashMap<Int, ComicStrip>> {
        return try {

            val comic = comicService.getRecentComicItem()
            comic.isLoaded = true

            var count = comic.num
            val comicList =
                List(count) { if (it == count) comic else ComicStrip(num = count--) }

            return Response.success(comicList.associateBy({ it.num }, { it }) as HashMap)
        } catch (e: Exception) {
            handleError(e)
        }
    }

    suspend fun getComicFromNetwork(num: Int): Response<ComicStrip> {
        return try {

            val comic = comicService.getComicItem(num)
            comic.isLoaded = true
            return Response.success(
                comic
            )
        } catch (e: Exception) {
            handleError(e)
        }
    }

//    // TODO: Check if there is an API to fetch all comics metadata at once
//    suspend fun getComicsFromNetwork(): Response<List<ComicStrip>> {
//        return try {
//            return Response.success(
//                comicService.getComicItems()
//            )
//        } catch (e: Exception) {
//            handleError(e)
//        }
//    }

    suspend fun saveComicsToDB(comics: List<ComicStrip>) {
        comicDao.insertAll(comics)
    }


    suspend fun updateComicInDB(comic: ComicStrip) {
        comicDao.update(comic)
    }

    private fun <R : Any> handleError(e: Exception): Response<R> {
        return Response.error(
            e.message ?: "Unknown Exception", null
        )
    }

}
