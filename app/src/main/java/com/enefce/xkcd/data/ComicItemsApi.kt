package com.enefce.xkcd.data

import com.enefce.xkcd.data.model.ComicStrip
import retrofit2.http.GET
import retrofit2.http.Path

interface ComicItemsApi {

    @GET("info.0.json")
    suspend fun getRecentComicItem(): ComicStrip

    @GET("{comic_id}/info.0.json")
    suspend fun getComicItem(@Path("comic_id") num: Int): ComicStrip
}

