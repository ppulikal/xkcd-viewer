package com.enefce.xkcd.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.enefce.xkcd.data.model.ComicStrip
import com.enefce.xkcd.utils.DatabaseWorker
import com.enefce.xkcd.utils.ListTypeConverter

/**
 * The Room database for this app
 */
@Database(
    entities = [ComicStrip::class],
    version = 1, exportSchema = false
)
@TypeConverters(ListTypeConverter::class)
abstract class ComicDatabase : RoomDatabase() {

    abstract fun comicDao(): ComicDao

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: ComicDatabase? = null

        fun getInstance(context: Context): ComicDatabase {
            return instance ?: synchronized(this) {
                instance
                    ?: buildDatabase(
                        context
                    )
                        .also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): ComicDatabase {
            return Room.databaseBuilder(context, ComicDatabase::class.java, "comics-db")
                .addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        val request = OneTimeWorkRequestBuilder<DatabaseWorker>().build()
                        WorkManager.getInstance(context).enqueue(request)
                    }
                })
                .build()
        }
    }
}