package com.enefce.xkcd.data

import com.enefce.xkcd.data.model.ComicStrip
import retrofit2.Retrofit

class ComicItemService(private val retrofit: Retrofit) :
    ComicItemsApi {
    private val comicItemApi by lazy { retrofit.create(ComicItemsApi::class.java) }
    override suspend fun getRecentComicItem() = comicItemApi.getRecentComicItem()
    override suspend fun getComicItem(num: Int): ComicStrip = comicItemApi.getComicItem(num)
}
