package com.enefce.xkcd.features.comics.list

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.enefce.xkcd.data.ComicItemsRepository
import com.enefce.xkcd.data.Response
import com.enefce.xkcd.data.Status
import com.enefce.xkcd.data.model.ComicStrip
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ComicListViewModel(private val comicItemsRepository: ComicItemsRepository) : ViewModel(
) {
    private val TAG = "ComicListViewModel"

    var comicItemsLiveData: MutableLiveData<HashMap<Int, ComicStrip>> = MutableLiveData()
    var comicItemLiveData: MutableLiveData<ComicStrip> = MutableLiveData()
    var errorLiveData: MutableLiveData<String> = MutableLiveData()

    fun comics() = comicItemsLiveData.value
    fun comic() = comicItemLiveData.value
    fun error() = errorLiveData.value

    fun getComics() {
        fetchComics(databaseQuery = { comicItemsRepository.getComicsFromDB() },
            networkCall = { comicItemsRepository.getComicsFromNetwork() },
            saveCallResult = { comicItemsRepository.saveComicsToDB(it.values.toList()) })
    }


    private fun <T> fetchComics(
        databaseQuery: suspend () -> Response<T>,
        networkCall: suspend () -> Response<T>,
        saveCallResult: suspend (T) -> Unit
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val source = databaseQuery.invoke()
            comicItemsLiveData.postValue(source.data as HashMap<Int, ComicStrip>)
            if ((source.data as HashMap<Int, ComicStrip>).size == 0) {
                val response = networkCall.invoke()
                if (response.status == Status.SUCCESS) {
                    response.data?.let {
                        saveCallResult(it)
                        comicItemsLiveData.postValue(it as HashMap<Int, ComicStrip>)
                    }

                } else if (response.status == Status.ERROR) {
                    errorLiveData.postValue(response.message)
                }
            }
        }
    }

    fun saveComic(comic: ComicStrip) {
        Log.d(TAG, "inside saveComic() : ${comic.num}")

        viewModelScope.launch(Dispatchers.IO) {
            comicItemsRepository.updateComicInDB(comic)
            comicItemLiveData.postValue(comic)
        }
    }

    fun getComic(num: Int) {
        Log.d(TAG, "inside getComic() : $num")
        viewModelScope.launch(Dispatchers.IO) {
            val response = comicItemsRepository.getComicFromNetwork(num)
            if (response.status == Status.SUCCESS) response.data?.let {
                saveComic(it)
            } //else ignore
        }
    }


}
