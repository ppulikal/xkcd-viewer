package com.enefce.xkcd.features.comics.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ShareCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.enefce.xkcd.R
import com.enefce.xkcd.data.model.ComicStrip
import com.enefce.xkcd.features.comics.list.ComicListViewModel
import kotlinx.android.synthetic.main.comic_detail.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ComicDetailFragment
    : Fragment() {

    companion object {
        /**
         * The fragment argument representing the Comic ID that this fragment
         * represents.
         */
        const val ARG_ID = "comic_id"

    }

    private val TAG = "ComicDetailFragment"
    private val viewModel: ComicListViewModel by sharedViewModel() //TODO: for now sharing the list viewmodel, change in future
    private var comic: ComicStrip? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle ->
            if (viewModel.comics()?.size != 0) {
                comic = viewModel.comics()?.get(
                    if (bundle.containsKey(ARG_ID)) bundle.getInt(ARG_ID) else viewModel.comics()?.size
                )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.comic_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let {
            Glide.with(it)
                .load(comic?.img)
                .placeholder(R.drawable.ic_baseline_image_24)
                .into(img_comic_detail)
        }
        comic?.let {
            detail_title.text = it.title
            detail_desc.text = it.alt
            img_detail_fav.setImageResource(if (it.isFavorite) R.drawable.ic_baseline_favorite_24 else R.drawable.ic_baseline_favorite_border_24)

            img_detail_fav.setOnClickListener { view ->
                it.isFavorite = !it.isFavorite
                viewModel.saveComic(it)
                img_detail_fav.setImageResource(if (it.isFavorite) R.drawable.ic_baseline_favorite_24 else R.drawable.ic_baseline_favorite_border_24)
            }

            img_detail_share.setOnClickListener { view ->
                shareComic(it.img)
            }

            img_detail_explain.setOnClickListener { view ->
                showExplanation(it.num)
            }
        }
    }


    private fun shareComic(url: String) {
        Log.d(TAG, "inside shareComic() : ${url}")

        activity?.let {
            ShareCompat.IntentBuilder.from(it)
                .setType("text/plain")
                .setChooserTitle("Select a medium")
                .setText("Check this interesting toon from xkcd:  $url")
                .startChooser()
        };
    }

    private fun showExplanation(id: Int) {
        Log.d(TAG, "inside showExplanation() : $id")
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.explainxkcd.com/${id}")))
    }

}
