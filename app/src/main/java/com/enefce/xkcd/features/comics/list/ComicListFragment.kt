package com.enefce.xkcd.features.comics.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import com.enefce.xkcd.R
import com.enefce.xkcd.data.model.ComicStrip
import com.enefce.xkcd.features.comics.detail.ComicDetailFragment
import com.enefce.xkcd.utils.navigate
import kotlinx.android.synthetic.main.comic_list.*
import kotlinx.android.synthetic.main.comic_list_fragment.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class ComicListFragment : Fragment() {
    private val TAG = "ComicListFragment"

    private var twoPane: Boolean = false
    private val comicListViewModel: ComicListViewModel by sharedViewModel()

    private val comicListObserver = Observer<HashMap<Int, ComicStrip>> {
        handleListUpdate(it)
    }

    private val comicObserver = Observer<ComicStrip> {
        handleListUpdate(it)
    }

    private val errorObserver = Observer<String> {
        showError(it)
    }

    private var comicItemsAdapter: ComicItemsRecyclerViewAdapter? = null
    private val onListItemClickListener: View.OnClickListener

    init {
        onListItemClickListener = View.OnClickListener { v ->
            val item = v.tag as ComicStrip
            if (twoPane) {
                val fragment = ComicDetailFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ComicDetailFragment.ARG_ID, item.num)
                    }
                }
                activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.comic_detail_container, fragment)?.commit()
            } else {
                showComicDetails(Bundle().apply {
                    putInt(ComicDetailFragment.ARG_ID, item.num)
                })
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.comic_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        twoPane = (comic_detail_container != null)
        setupRecyclerView(comic_list)
        setupScrollListener(comic_list)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        comicListViewModel.comicItemsLiveData.observe(viewLifecycleOwner, comicListObserver)
        comicListViewModel.comicItemLiveData.observe(viewLifecycleOwner, comicObserver)
        comicListViewModel.errorLiveData.observe(viewLifecycleOwner, errorObserver)
        comicListViewModel.getComics()
    }


    private fun setupScrollListener(recyclerView: RecyclerView?) {
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(
                recyclerView: RecyclerView,
                newState: Int
            ) {
                comicItemsAdapter?.rv_scroll_state = newState
                if (newState == SCROLL_STATE_IDLE) recyclerView.adapter?.notifyDataSetChanged()
            }
        })
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        comicItemsAdapter = ComicItemsRecyclerViewAdapter(
            comicListViewModel.comics() ?: hashMapOf<Int, ComicStrip>(), onListItemClickListener,
            comicListViewModel
        )
        recyclerView.adapter = comicItemsAdapter
    }

    private fun handleListUpdate(comics: HashMap<Int, ComicStrip>) {
        Log.d(TAG, "SUCCESS - Inside handleListUpdate - ${comics.size} $comicItemsAdapter")
        comicItemsAdapter?.values = comics
        comicItemsAdapter?.notifyDataSetChanged()
        hideProgress()
        hideError()

        if (twoPane) {
            val fragment = ComicDetailFragment().apply {
                arguments = Bundle().apply {
                    val count = comicListViewModel.comics()?.size ?: 0
                    putInt(ComicDetailFragment.ARG_ID, count)
                }
            }
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.comic_detail_container, fragment)?.commit()
        }
    }

    private fun handleListUpdate(comicStrip: ComicStrip) {
        Log.d(TAG, "SUCCESS - Inside handleListUpdate Single Comic - ${comicStrip.num}")
        comicItemsAdapter?.values?.replace(comicStrip.num, comicStrip)
        comicItemsAdapter?.notifyDataSetChanged() //TODO: check if the single item can be inserted and notifyItemChanged() be called

    }

    private fun showComicDetails(bundle: Bundle) {
        navigate(R.id.detailFragment, bundle)
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        progress.visibility = View.INVISIBLE
    }

    private fun hideError() {
        error_tv.visibility = View.INVISIBLE
    }

    private fun showError(message: String) {
        Log.d(TAG, "ERROR - $message")
        hideProgress()
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}
