package com.enefce.xkcd.features.comics.list

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enefce.xkcd.R
import com.enefce.xkcd.data.model.ComicStrip
import kotlinx.android.synthetic.main.comic_list_content.view.*


class ComicItemsRecyclerViewAdapter(
    var values: HashMap<Int, ComicStrip>,
    private val onClickListener: View.OnClickListener,
    private val comicListViewModel: ComicListViewModel,
    var rv_scroll_state: Int = RecyclerView.SCROLL_STATE_IDLE
) :
    RecyclerView.Adapter<ComicItemsRecyclerViewAdapter.ViewHolder>() {
    private val TAG = "ComicItemsRecyclerViewAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.comic_list_content, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (!values.containsKey(values.size - (position))) return
        val item = values.getValue(values.size - (position))
        if (item != null) {
            holder.num.text = "#${item.num}"
            holder.title.text = item.title
            holder.date.text = "${item.day}-${item.month}-${item.year}"
            holder.fav.visibility = if (item.isFavorite) View.VISIBLE else View.INVISIBLE

            with(holder.itemView) {
                tag = item
                setOnClickListener(onClickListener)
            }

            if (!item.isLoaded &&
                (rv_scroll_state == RecyclerView.SCROLL_STATE_IDLE)
            ) {
                loadData(item.num)
            }
        }
    }

    private fun loadData(num: Int) {
        Log.d(TAG, "Loading Data $num")
        comicListViewModel.getComic(num)
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val num: TextView = view.num
        val title: TextView = view.title
        val date: TextView = view.date
        val fav: ImageView = view.fav
    }
}

