package com.enefce.xkcd


import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

/*
Single Activity approach used throughout the application
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu) //TODO: Implement Search and info
        return true
    }

    //TODO: Implement Search and info actions
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> Toast.makeText(this, "TODO: Search Action", Toast.LENGTH_LONG)
                .show()
            R.id.action_info -> Toast.makeText(this, "TODO: Show Info", Toast.LENGTH_LONG).show()
            else -> return super.onOptionsItemSelected(item);
        }
        return true
    }
}