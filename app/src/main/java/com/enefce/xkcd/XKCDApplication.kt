package com.enefce.xkcd

import androidx.multidex.MultiDexApplication
import com.enefce.xkcd.di.appModules
import com.enefce.xkcd.di.networkModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class XKCDApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@XKCDApplication)
            modules(listOf(appModules, networkModules))
        }
    }
}