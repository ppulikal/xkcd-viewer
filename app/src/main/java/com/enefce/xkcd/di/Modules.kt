package com.enefce.xkcd.di

import com.enefce.xkcd.BuildConfig
import com.enefce.xkcd.data.ComicDatabase
import com.enefce.xkcd.data.ComicItemService
import com.enefce.xkcd.data.ComicItemsApi
import com.enefce.xkcd.data.ComicItemsRepository
import com.enefce.xkcd.features.comics.list.ComicListViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModules = module {

    single { ComicDatabase.getInstance(get()).comicDao() }

    single {
        ComicItemService(get())
    }
    single {
        ComicItemsRepository(get(), get())
    }
    viewModel {
        ComicListViewModel(get())
    }
}


val networkModules = module {
    factory { provideOkHttpClient(get()) }
    factory { provideLoggingInterceptor() }
    factory { provideComicItemsApi(get()) }
    single { provideRetrofit(get()) }
}


fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.SERVER_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(
    loggingInterceptor: HttpLoggingInterceptor
): OkHttpClient {

    return OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build()
}

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val logger = HttpLoggingInterceptor()
    logger.level = HttpLoggingInterceptor.Level.BODY
    return logger
}


internal fun provideComicItemsApi(retrofit: Retrofit): ComicItemsApi =
    retrofit.create(ComicItemsApi::class.java)

