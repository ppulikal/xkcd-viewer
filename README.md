![Android CI](https://img.shields.io/bitbucket/pipelines/ppulikal/xkcd-viewer/master?style=plastic)


### Requirements: 
 -----------
    * browse through the comics,
        - implemented
    * see the comic details, including its description,
        - implemented
    * search for comics by the comic number as well as text,
        - Out of Scope for MVP. Coming in next version.
    * get the comic explanation
        - implemented - Launchin external browser linke
    * favorite the comics, which would be available offline too,
        - implemented (saving of details, not full image)
    * send comics to others,
        - implemented sharing.
    * get notifications when a new comic is published,
        - Out of Scope for MVP. Coming in next version.
    * support multiple form factors.
        - Customised for Phones and Tablets
    
    
# Implementation

## Features
### Implemented 
 -----------
    - Master / Detail Fragments
    - Single Activity
    - Phone and Tablet modes.
    - Network Fetch and save into Database
    - Lazy loading of comic items and saved into DB
    - Favorite a comic (only comic details are saved for offline mode. not full image)
    - Explanation - for now launching external browser, Check if an URL can be launched inside a Webview.
    - Share comic image url

## Tests
 -----------
    - Simple Espresso tests
    - Tests for ListViewModel and ComicItems Repositories
 

## TODO
 -----------
    - Zoom In / Zoom Out comic image in Detail screen.
    - Loading Animations
    - Show info
    - Need to check best possible mechanism for saving full favorite image. 
    - Notifications - Skipped for current MVP
    - Search feature - Skipped for current MVP

    
## Thirdparty libraries used
 -----------
    - Retrofit, OkHttp, Glide, Koin

## Architecture
 -----------
    - MVVM, LiveData, Room, Repository pattern
 
## Networking
 -----------
    - Retrofit, OkHttp
 
## Components Used
 -----------
   * Navigation component
   * Kotlin Coroutines
   * ViewModel
   * LiveData
   * Retrofit
   * Koin - Dependency Injection
   * Room Database
 
 
## Screenshots
 -----------
  - attached inside the Screenshots folder


![HomeScreen - Phone](screenshots/Phone-1.jpg "List Screen - Phone")
![DetailScreen - Phone](screenshots/Phone-2.jpg "Detail Screen - Phone")

![HomeScreen - Tablet](screenshots/Tablet-1.jpg "Home Screen - Tablet")


License
-------
MIT License

Copyright (c) 2020 Prasad Pulikal

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.